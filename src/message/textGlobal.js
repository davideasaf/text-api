import { successfulResponse, errorResponse } from '../util/response';
import { validMessageBody } from '../util/validators';
import { getAllUsers } from '../util/db';
import { sendMessage } from '../util/text';

export default async (event, context, cb) => {
  const { messageBody } = JSON.parse(event.body);
  if (!validMessageBody(messageBody)) {
    errorResponse(400, 'messageBody must be a string and not empty', cb);
    return;
  }

  try {
    const { Items: users } = await getAllUsers();
    console.log('users:', JSON.stringify(users, null, 2));
    users.forEach((user) => {
      console.log(`texting ${user.userName} at ${user.phoneNumber} with ${process.env.twilioFromNumber}`);
      sendMessage(user.phoneNumber, messageBody);
    });
    successfulResponse('success', cb);
  } catch (e) {
    console.error(e, e.stack);
    errorResponse(500, `Could not send global message: ${e}`, cb);
  }
};
