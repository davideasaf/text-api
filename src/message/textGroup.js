import AWS from 'aws-sdk'; // eslint-disable-line

import { successfulResponse, errorResponse } from '../util/response';
import { validMessageBody, validGroupName } from '../util/validators';
import { getAllUsersInGroup } from '../util/db';
import { sendMessage } from '../util/text';


export default async (event, context, cb) => {
  const { messageBody, group } = JSON.parse(event.body);
  if (!validMessageBody(messageBody)) {
    errorResponse(400, 'messageBody must be a string and not empty', cb);
    return;
  }
  if (!validGroupName(group)) {
    errorResponse(400, 'group must be a string', cb);
    return;
  }

  try {
    const { Items: usersInGroup } = await getAllUsersInGroup(group);
    console.log('getAllUsersInGroup Items:', JSON.stringify(usersInGroup, null, 2));
    usersInGroup.forEach((user) => {
      console.log(`texting ${user.userName} at ${user.phoneNumber} with ${process.env.twilioFromNumber}`);
      sendMessage(user.phoneNumber, messageBody);
    });
    successfulResponse('success', cb);
  } catch (e) {
    console.error(e, e.stack);
    errorResponse(500, `there was error sending messages to these groups: ${group}\n${e}`, cb);
  }
};
