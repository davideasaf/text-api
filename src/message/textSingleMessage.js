import AWS from 'aws-sdk'; // eslint-disable-line

import { successfulResponse, errorResponse } from '../util/response';
import { validMessageBody, validPhoneNumber } from '../util/validators';
import { sendMessage } from '../util/text';

export default async (event, context, cb) => {
  const { messageBody, phoneNumber } = JSON.parse(event.body);
  if (!validMessageBody(messageBody)) {
    errorResponse(400, 'messageBody must be a string and not empty', cb);
    return;
  }
  console.log('phoneNumber:', phoneNumber);
  if (!validPhoneNumber(phoneNumber)) {
    errorResponse(400, 'phone number must be in the correct format of 11 digits: 00000000000', cb);
    return;
  }

  console.log(`texting ${phoneNumber} with ${process.env.twilioFromNumber}`);
  try {
    sendMessage(`+${phoneNumber}`, messageBody);
  } catch (e) {
    console.log('Error sending message via Twillio:', e);
    errorResponse(400, `Error sending message via Twillio: ${e}`, cb);
  }

  successfulResponse('success', cb);
};
