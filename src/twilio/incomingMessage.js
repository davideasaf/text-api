import AWS from 'aws-sdk'; // eslint-disable-line
import _get from 'lodash/get';

import { successfulTextResponse, errorResponse } from '../util/response';
import { getGroupName, sendMessage, getGroupAdminNum } from '../util/text';

const { USERS_TABLE } = process.env;
const dynamoDb = new AWS.DynamoDB.DocumentClient();

export default (event, context, cb) => {
  console.log('Received event:', JSON.stringify(event, null, 2));
  const receivedMessageBody = event.queryStringParameters.Body.trim();
  const fromNumber = event.queryStringParameters.From;

  // Check if new user
  if (receivedMessageBody.toLowerCase() === 'young' || receivedMessageBody.toLowerCase() === 'harmony') {
    const groupShortName = receivedMessageBody;
    // Store Number
    // Create DynamoDb params Query
    const params = {
      Item: { phoneNumber: fromNumber, groups: [getGroupName(groupShortName)] },
      TableName: USERS_TABLE,
    };
    dynamoDb.put(params, (err) => {
      if (err) {
        errorResponse(500, err, cb);
        return;
      }
      // Send Welcome Message
      sendMessage(fromNumber, 'Welcome to the Graceway Young Adults Group!', (error) => {
        if (error) {
          errorResponse(500, err, cb);
          return;
        }
        successfulTextResponse("Please send your name and we'll make sure you get all our awesome updates!", cb);
      });
    });
  } else {
    const queryParams = {
      TableName: USERS_TABLE,
      Key: {
        phoneNumber: fromNumber,
      },
    };
    console.log('About to queryDynamoDB...');
    queryDynamoDB(queryParams, (err, queryRes) => {
      console.log('queryRes:', JSON.stringify(queryRes, null, 2));
      const userName = _get(queryRes, 'Item.userName');
      const groupName = _get(queryRes, 'Item.groups[0]');
      if (userName) {
        // Send response to Admin
        const messageToAdmin = `${userName} says: ${receivedMessageBody}`;
        sendMessage(getGroupAdminNum(groupName), messageToAdmin, (error) => {
          if (error) {
            console.error('ERROR:', error);
            return;
          }
          successfulTextResponse(`Thanks ${userName}. I'll send that response right over!`, cb);
        });
      } else if (queryRes.Item.phoneNumber) {
        // Update Name
        const newUserName = receivedMessageBody;
        const updateParams = {
          TableName: USERS_TABLE,
          Key: { phoneNumber: fromNumber },
          UpdateExpression: 'set userName = :userName',
          ExpressionAttributeValues: { ':userName': newUserName },
        };
        dynamoDb.update(updateParams, (error) => {
          if (error) {
            console.error('ERROR:', error);
          } else {
            successfulTextResponse(`Thanks ${newUserName}! You'll now be notified of the Young Adults Group events!`, cb);
          }
        });
      } else {
        successfulTextResponse('hmmm.. I don\'t quite know that command yet. Ask David Asaf about it...', cb);
      }
    });
  }
};
