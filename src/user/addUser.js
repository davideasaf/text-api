import AWS from 'aws-sdk'; // eslint-disable-line
import isEmpty from 'lodash/isEmpty';
import { successfulResponse, errorResponse } from '../util/response';
import { validPhoneNumber } from '../util/validators';
import { addUser } from '../util/db';

export default async (event, context, cb) => {
  const { phoneNumber, userName, groups } = JSON.parse(event.body);
  if (typeof userName !== 'string') {
    errorResponse(400, '"userName" must be a string', cb);
    return;
  }
  if (!validPhoneNumber(phoneNumber)) {
    errorResponse(400, 'phone number must be in the correct format of 11 digits: 00000000000', cb);
    return;
  }
  if (!Array.isArray(groups) || isEmpty(groups)) {
    errorResponse(400, 'groups must be an array with at least one group name', cb);
    return;
  }

  try {
    const addUserResponse = await addUser({ phoneNumber, userName, groups });
    console.log('success!');
    successfulResponse(addUserResponse, cb);
  } catch (e) {
    console.error(e);
    errorResponse(500, `Could not create user: ${e}`, cb);
  }
};
