import { get } from 'lodash';
import { successfulResponse, errorResponse } from '../util/response';
import { validUuid } from '../util/validators';
import { getUserByUserId, getAllUsers } from '../util/db';

export default async (event, context, cb) => {
  const userId = get(event, 'queryStringParameters.userId');
  if (userId) {
    if (!validUuid(userId)) {
      errorResponse(400, 'userId must be provided as a valid uuid', cb);
      return;
    }
    try {
      const { Item: user } = await getUserByUserId(userId);
      successfulResponse(user, cb);
    } catch (e) {
      console.log(e, e.stack);
      errorResponse(400, `Could not retrieve user by id: ${userId}. ${e}`, cb);
    }
  } else {
    try {
      const { Items: users } = await getAllUsers();
      successfulResponse(users, cb);
    } catch (e) {
      console.log(e, e.stack);
      errorResponse(400, `Could not retrieve all users: ${e}`, cb);
    }
  }
};
