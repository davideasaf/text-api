import AWS from 'aws-sdk'; // eslint-disable-line

import { successfulResponse, errorResponse } from '../util/response';
import { validMessageBody, validUuid } from '../util/validators';
import { getUserByUserId } from '../util/db';
import { sendMessage } from '../util/text';

export default async (event, context, cb) => {
  const { userId, messageBody } = JSON.parse(event.body);
  if (!validMessageBody(messageBody)) {
    errorResponse(400, 'messageBody must be a string and not empty', cb);
    return;
  }
  if (!validUuid(userId)) {
    errorResponse(400, 'userId must be provided as a valid uuid', cb);
    return;
  }

  try {
    const { Item: user } = await getUserByUserId(userId);
    sendMessage(user.phoneNumber, messageBody);
  } catch (e) {
    console.error(`Error sending user: ${userId} a message: ${e}`);
    errorResponse(400, `Error sending user: ${userId} a message: ${e}`, cb);
  }

  successfulResponse('success', cb);
};
