import AWS from 'aws-sdk'; // eslint-disable-line
import uuid from 'uuid/v4';

const { USERS_TABLE } = process.env;
const dynamoDb = new AWS.DynamoDB.DocumentClient();

export const addUser = ({ phoneNumber, userName, groups }) => dynamoDb.put({
  TableName: USERS_TABLE,
  Item: {
    userId: uuid(),
    phoneNumber: `+${phoneNumber}`,
    userName,
    groups,
  },
}).promise();

export const getUserByUserId = userId => dynamoDb.get({ TableName: USERS_TABLE, Key: { userId } })
  .promise();

export const getAllUsers = () => dynamoDb.scan({ TableName: USERS_TABLE }).promise();

export const getAllUsersInGroup = group => dynamoDb.scan({
  TableName: USERS_TABLE,
  ScanFilter: {
    groups: {
      ComparisonOperator: 'CONTAINS',
      AttributeValueList: [group],
    },
  },
}).promise();
