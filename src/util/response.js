import twilio from 'twilio';

const { MessagingResponse } = twilio.twiml;

export const successfulResponse = (jsonResponse, cb) => cb(null, {
  statusCode: 200,
  headers: {
    /* Required for CORS support to work */
    'Access-Control-Allow-Origin': '*',
    /* Required for cookies, authorization headers with HTTPS */
    'Access-Control-Allow-Credentials': true,
  },
  body: JSON.stringify(jsonResponse),
});

export const successfulTextResponse = (messageResponse, cb) => {
  const twiml = new MessagingResponse();
  twiml.message(messageResponse);

  cb(null, {
    statusCode: 200,
    headers: {
      /* Required for CORS support to work */
      'Access-Control-Allow-Origin': '*',
      /* Required for cookies, authorization headers with HTTPS */
      'Access-Control-Allow-Credentials': true,
      'Content-Type': 'text/xml',
    },
    body: twiml.toString(),
  });
};

export const errorResponse = (statusCode, message, cb) => cb(null, {
  statusCode,
  headers: {
    /* Required for CORS support to work */
    'Access-Control-Allow-Origin': '*',
    /* Required for cookies, authorization headers with HTTPS */
    'Access-Control-Allow-Credentials': true,
  },
  body: JSON.stringify({
    message,
  }),
});
