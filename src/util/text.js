import twilio from 'twilio';

const twilioClient = twilio(process.env.ACCOUNT_SID, process.env.AUTH_TOKEN);
const { twilioFromNumber } = process.env;

export const sendMessage = (toNumber, messageBody) => twilioClient.messages.create({
  to: toNumber,
  from: twilioFromNumber,
  body: messageBody,
});

export const getGroupName = (shortName) => {
  const groupMap = {
    harmony: 'inHarmony',
    young: 'youngAdults',
  };
  return groupMap[shortName];
};

export const getGroupAdminNum = (groupName) => {
  const groupMap = {
    inHarmony: '+15005550006',
    youngAdults: '+19253259538',
  };
  return groupMap[groupName];
};
