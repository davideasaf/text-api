const validFormats = {
  phoneNumber: /^\d{11}$/,
  uuid: /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i,
};

export const validPhoneNumber = phoneNumber => validFormats.phoneNumber.test(phoneNumber);

export const validUuid = uuid => validFormats.uuid.test(uuid);

export const validMessageBody = body => body && typeof body === 'string' && body.length > 0;

export const validGroupName = groupName => groupName && typeof groupName === 'string' && groupName.length > 0;
