import request from './exampleRequests/getUserRequest';
import getUsers from '../src/user/getUsers';

describe('#getUser', () => {
  it('no userId returns all users', () => {
    request.queryStringParameters = {};
    getUsers(request, {}, (err, response) => {
      console.error(err);
      console.log(response.body);
    });
  });
  it('successfully gets table response', () => {
    getUsers(request, {}, (err, response) => {
      console.error(err);
      console.log(response.body);
    });
  });
});
