import textGroup from '../src/message/textGroup';
import text from '../src/util/text';

describe('#test', () => {
  it('no userId returns all users', async () => {
    sinon.stub(text, 'sendMessage').returns({});
    const response = await textGroup({ event: { body: { messageBody: 'test', group: 'youndAdults' } } }, {}, {});
    console.log(response);
  });
});
